import Vue from 'vue';
/* eslint-disable */
//import scss from './styles/app.scss';
import { store } from './store/store.js';
import Router from './router.js';

import LazyLoadDirective from './directives/LazyLoadDirective';

import App from './App.vue';

import SvgIcon from './patterns/atoms/icon/icon.vue';

Vue.component('svg-icon', SvgIcon);
Vue.directive('lazyload', LazyLoadDirective);

new Vue({
  el: "#app",
  router: Router,
  store: store,
  render: h => h(App)
});
/* eslint-enable */
