const env = process.env.APP_ENV || 'development';

const config = {
  gtmId: '',
};

const localizedConfigs = {
  production: require('./production').default,
  development: require('./development').default,
};

const localizedConfig = localizedConfigs[env];

export default Object.assign(config, localizedConfig);
