export default {
  self: {
    url: 'https://wouterborst.com',
  },
  api: {
    url: 'https://wouterborst.com/api',
    website: 'https://wouterborst.com',
    version: '1.0',
  },
  website: {
    url: 'https://wouterborst.com',
  },
};
