export default {
  self: {
    url: 'https://localhost:8080',
  },
  api: {
    url: 'https://localhost:8080/api',
    website: 'https://localhost:8080',
    version: '1.0',
  },
  website: {
    url: 'https://localhost:8080',
  },
};
