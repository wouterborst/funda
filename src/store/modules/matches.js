import Axios from 'axios';

// Define here API Key and Id
const apikey = 'ac1b0b1572524640a0ecc54de453ea9f';
// const apiId = '6289a7bb-a1a8-40d5-bed1-bff3a5f62ee6';

// The initial state
const State = {
  matches: [],
  matchCount: null,
  matchSuggestions: [],
};

// Getters get stuff from the store.
// This is where you can modify data before sending it to your component.
const Getters = {
  matches: state => state.matches,
  matchSuggestions: state => state.matchSuggestions,
  matchCount: state => state.matchCount,
};

/* eslint-disable */
// Mutations change the actual data in the state
const Mutatuons = {    
  populate: (state, data) => {
    state.matchSuggestions = data;
  },  
  increment(state) {
    // mutate state
    state.matchCount++;
  },
  set(state, currentMatch) {
    state.matches.push(currentMatch);
  }
};
/* eslint-enable */

// Actions don't modify the state directly,
// but instead handle more complex logic and then commit something to the state using mutations
const Actions = {
  get({ commit }) {
    return Axios.get(`http://partnerapi.funda.nl/feeds/Aanbod.svc/${apikey}?type=koop&zo=/amsterdam/tuin/video/`).then(response => {
      // Only commit the Objects of the Funda API
      commit('populate', response.data.Objects);
    });
  },
  increment(context) {
    context.commit('increment');
  },
  updateMatches({ commit }, currentMatch) {
    commit('set', currentMatch);
  },
};

// Export everything.
export default {
  state: State,
  getters: Getters,
  mutations: Mutatuons,
  actions: Actions,
};
