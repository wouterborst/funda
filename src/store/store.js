import Vue from 'vue';
import Vuex from 'vuex';

import matches from './modules/matches';

/* eslint-disable */
Vue.use(Vuex);
export const store = new Vuex.Store({
  modules: {
    matches,
  }
});
/* eslint-enable */
