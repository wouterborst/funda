/*
 * General imports
 */
/* eslint-disable */

import Vue from 'vue';
import Router from 'vue-router';

/*
 * Import views
 */
import Index from "./pages/index.vue";
import Matches from "./pages/matches.vue";
import Single from "./pages/single.vue";
import NotFound from "./pages/404.vue";

const siteName = ' @ Funda';

// Use router
Vue.use(Router);

// Setup routes
export default new Router({
  mode: "history",
  routes: [
    { path: '*', 
      component: NotFound,
      meta: {
        title: 'Not found' + siteName,
      }
    },
    {
      name: "Index",
      path: "/",
      component: Index,
      meta: {
        title: 'Select your realestate' + siteName,
      }
    },
    {
      name: "Matches",
      path: "/matches",
      component: Matches,
      meta: {
        title: 'Matches' + siteName,
      }
    },
    {
      path: "/:id",
      name: "Single",
      component: Single,
      meta: {
        title: 'Woning' + siteName,
      }
    }
  ],
  scrollBehavior(to, from, savedPosition) {
    return { x: 0, y: 0 }
  },
});

/* eslint-enable */
